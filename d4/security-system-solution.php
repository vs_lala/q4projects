<?php include 'layout/_header.php'; 
	// $activeProjects = 'active';
	$activeSecurity = 'active';
?>

<body>
	<?php include 'layout/_navbar.php'; ?>
	<br/><br/>

	<div class="container">
		<div class="center wow fadeInDown">
            <h2>Security Solutions</h2>
            <p class="lead">Your Safety is our Responsibility</p>
        </div>

        <div class="col-md-12 col-sm-12 center wow fadeInDown">
        	<h2 >Fire Hydrant</h2>
        	<center><img src="images/service/hydrant.jpg" class="img-responsive" ></center>
        	<p>Q4Projects. These systems are designed to act as a source which supplies water with municipal water service. Usually, the product is used for providing water to most urban, suburban and rural areas to help firefighters tap municipal water and execute the fire extinguishing operation. Our experts, keeping in mind the various safety purposes, design &amp; develop the offered <strong>Water Based Hydrant System</strong> for several industrial sectors. The required inputs like washers, metals, knobs etc. for the product are sourced from vendors who are highly trusted &amp; certified. Engrossing features like compact design, sturdy &amp; rust-proof body, easy installation &amp; extraction of water and reliable performance have provided this product the tag of being the best among the similar one's available in the market. Our professionals before dispatching the product, check is functioning &amp; durability at our quality testing unit to ensure its flawlessness. For our clients complete satisfaction, we make these hydrant systems available in different specifications at affordable prices.<br><br></p>
        </div>
        <a id="fire_hydrant"></a>

       	<div class="center wow fadeInDown">
            <h2>Electric Fences</h2>
            <p class="lead"></p>
        </div>

        <div class="col-md-12 col-md-12 center wow fadeInDown">
        	<center><img src="images/service/electric-fencing.jpg" class="img-responsive" /></center><br/>
        	<p>We offer the Invisible Fence<sup>®</sup> Brand family of safety solutions to ensure the best quality, performance and reliability. Our primary objective is to keep your dogs and cats happy, healthy and safe…all the time... and in so doing, bring a better quality of life for you, your family and your pets.</span></p>
        	<p>Moriarty’s Fence Company is committed to excellence. The Company has achieved and surpassed industry customer service standards consistently for 27 years. &nbsp;<strong>Moriarty’s Fence Company</strong> has been family owned and operated since 1986 - we invite you to&nbsp;<a href="/LinkClick.aspx?link=93&amp;tabid=56&amp;portalid=0&amp;mid=369"><strong>discover our history</strong></a>&nbsp;of personal service and dedication to the safety of your family's pets.</span></p>
        </div>
        <a id="electric_fence" ></a>
        <div class="center wow fadeInDown">
        	<h2>CCTV Surveillance</h2>
        	<p class="lead">We will keep a watch, Always...</p>
        </div>

        <div class="col-md-12 col-sm-12 center wow fadeInDown">
        	<center><img src="images/service/cctv.jpg" class="img-responsive"></center>
        	<p>DAccess offers a plethora of superior quality CCTV and surveillance systems to secure your buildings, offices, and homes. Our CCTV surveillance system offers the most optimum solution for indoor as well as outdoor surveillance. <br>
              Our range of surveillance systems  cater to a wide range of applications like homes, malls, offices, shops, schools and lots more.<br>
              <br>
              Security systems have been the most important facet for the protection of property, offices, etc.  DAccess offers stringent surveillance technology in all shapes and sizes, depending on client requirements with high resolution and cost effectiveness.<br>
              <br>
              <br>
              Our arsenal of CCTV surveillance includes the Dome camera, IR dome camera, PTZ Camera, joystick controller, hidden cameras and other state-of-the-art surveillance technology. </p>
        </div>
        <a  id="cctv"></a>
    </div>
</body>


<?php include 'layout/_footer.php'; ?>