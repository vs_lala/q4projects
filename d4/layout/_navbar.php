<header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +0123 456 70 90</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            </ul>
                            <div class="search">
                                <form role="form">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                           </div>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

<nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a> -->
                    <a class="navbar-brand" href="index.php"><h1>Q4Projects</h1></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="<?= $activeHome ?>"><a href="index.php">Home</a></li>
                        <li class="<?= $activeAbout ?>"><a href="about-us.php">About Us</a></li>
                        <li class="<?= $activeServices ?>"><a href="services.php">Services</a></li>
                        <li class="<?= $activeProjects ?>" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="security-system-solution.php#fire_hydrant">Fire Hydrant</a></li>
                                <li class="<?= $activeSoftwareDevelopment ?>"><a href="software-development.php">Software Development</a></li>
                                <li class="<?= $activeSecurity ?>"><a href="security-system-solution.php">Security Solution</a></li>
                                <li class="<?= $activeWebDesign ?>"><a href="web-design-development.php">Web Development</a></li>
                                <li><a href="security-system-solution.php#cctv">CCTV Camera System</a></li>
                                <li><a href="security-system-solution.php#electric_fence">Electric Fencing</a></li>
                                <li><a href="software-development.php#biometric">Biometric Time Attendence</a></li>
                                <li><a href="software-development.php#pa_system">PA System</a></li>
                                <li><a href="software-development.php#access_control">Access Control System</a></li>

                            </ul>
                        </li>
                        <li class="<?= $activeContact ?>"><a href="contact-us.php">Contact</a></li>                        
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        </header><!--/header-->