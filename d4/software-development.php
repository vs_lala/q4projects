<?php include 'layout/_header.php'; 
	// $activeProjects = 'active';
	$activeSoftwareDevelopment = 'active';
?>

<body>
	<?php include 'layout/_navbar.php'; ?>
	<br/><br/>
	<div class="container">
		<div class="center wow fadeInDown">
            <h2>Software Development</h2>
            <p class="lead">We love what we do</p>
        </div>
		
		<div class="row">
			<div class="col-md-6 col-sm-6 center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
				<h2>Customized Design & Developments</h2>
				<img src="images/services/software-design.png" class="img-responsive img-circle">
				<p>Q4Projects is a custom software development firm and has furnished various clients with an altered programming result outlined particularly to interface with their nature. Q4Projects comprehends that each organization or association is diverse and needs devices to oblige their society instead of adjusting a society around an instrument. Q4Projects methodology is streamlined from years of programming advancement encounter and envelops the information picked up from working with numerous distinctive associations. 
<br><br>
Q4Projects uses a five stage handle and starts with the Requirements Definition. This stage is the place we start the guide and archive the necessities sought by the client. This is finished by breaking down data picked up from our customers as records, existing frameworks, process details, and if needed on location investigation meetings with end clients.
        </p>
			</div>

			<div class="col-md-6 col-sm-6 center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
				<h2>Embedded Solutions<br/></h2>
				<img src="images/services/embed-system.png" class="img-responsive img-circle">
				<p>Q4Projects Security Solution is an incorporated service and solution provider in implanted technologies, giving embedded design and product development services to clients in India &amp; around the globe. <br><br>The center is on giving vigorous inserted results over a different industry fragments. We trust Cost effective Quality items consolidated with Quality Technical backing will be the first decision of our clients concerning picking an item for inserted improvement. 
<br><br>
Q4Projects Security Solutions is a consequence of our earnest exertion to bring Quality items at moderate costs.</p>
			</div>

		</div>

		<div class="row">
			<div class="col-md-6 col-sm-6 center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
			<h2 id="biometric">Biometric Time Attendence<br/></h2>
				<img src="images/portfolio/new/bio.png" class="img-responsive img-circle">
				<p>A Biometric Attendance System means ‘life measurement’, and it is mostly associated with security. Biometric identification has a broader relevance in terms of verification and identification of a human biometric sample. <br>
              <br>
              At DAccess, we focus on deployment of biometric products for measurements of unique personal characteristics to verify a person’s identity in real time, with time-in and time-out features and rock-solid security with superior and reliable technology. <br>
              <br>
              DAccess is a leading provider of biometric hardware and software systems with improved security, efficiency and high performance identity management. Our lineup of products include a Finger print vane detection based access control system, biometric based time and attendance system, EM locks, Finger print door locks and much more.<br>
           
            </p>
			</div>

			<div class="col-md-6 col-sm-6 center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
			<h2 id="pa_system">PA System<br/></h2>
				<img src="images/service/pa.jpg" class="img-responsive img-circle">
				<p>Q4Projects Security Solution is an incorporated service and solution provider in implanted technologies, giving embedded design and product development services to clients in India &amp; around the globe. <br><br>The center is on giving vigorous inserted results over a different industry fragments. We trust Cost effective Quality items consolidated with Quality Technical backing will be the first decision of our clients concerning picking an item for inserted improvement. 
<br><br>
Q4Projects Security Solutions is a consequence of our earnest exertion to bring Quality items at moderate costs.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-sm-6 center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
			<h2 id="access_control">Access Control System<br/></h2>
				<img src="images/portfolio/new/access.jpg" class="img-responsive img-circle">
				<p> We, at DAccess offer a plethora of <strong>Access Control Systems</strong> for private and government enterprises. Our products are designed to provide state of the art security solutions that seamlessly integrate with innovative technology solutions to deliver a versatile, reliable and stringent security infrastructure. <br>
              <br>
              <strong>Access Control Systems</strong> at DAccess are designed and developed on open flexible technology to provide real-time monitoring, management and control of your access system. 
              
              Our range of <strong>Access Control Systems in Pune, Mumbai, Hyderabad include the time-attendance payroll software</strong>, a standalone access controller,  Weigend reader, single door standalone access controller, Access Board connect 4 readers, Electromagnetic locks and the works!
              
              Deployment of centralized technology reduces cost and provides a solution to manage all your access points from a centralized location.
              <br>
            </p>
			</div>
		</div>
		
	</div>

</body>


<?php include 'layout/_footer.php'; ?>